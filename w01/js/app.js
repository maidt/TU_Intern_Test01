 jQuery(document).ready(function ($) {

    // ===================slider screen2=======================
         var swiper = new Swiper('.swiper-container.screen2', {
            pagination: '.swiper-pagination',
            slidesPerView: 5,
            paginationClickable: true,
            spaceBetween: 0,
            breakpoints: {
              375: {
                slidesPerView: 1,
                spaceBetween: 0
              },
              480: {
                slidesPerView: 2,
                spaceBetween: 0
              },
              992: {
                slidesPerView: 3,
                spaceBetween: 0
              },
              1125:{
                slidesPerView: 4,
                spaceBetween: 0
              }
            }
        });
         // end slider screen2 
    // ========================swiper slider screen3===========================

        var swiper = new Swiper('.swiper-container.screen3', {
          pagination: '.swiper-pagination',
          paginationClickable: true,
          effect: 'coverflow',
          grabCursor: true,
          centeredSlides: true,
          slidesPerView: 'auto',
          coverflow: {
              rotate: 0,
              stretch: 0,
              depth: 100,
              modifier: 1,
          }
      });
    // end slider screen3

      // --------------slider screen4--------
        var swiper = new Swiper('.swiper-container.screen4', {
          // pagination: '.swiper-pagination',
          paginationClickable: true,
          nextButton: '.swiper-button-next',
          prevButton: '.swiper-button-prev',
          parallax: true,
          speed: 600,
        });
      // end slider screen4
        var screen_width = $(window).width();
        if(screen_width > 1024 ){
           $('#fullpage').fullpage({
                sectionsColor: ['#fff', '#fff', '#f7f7f7', 'fff', '#fff'],
                anchors: ['trang chủ', '5 cách đặt mật khẩu', 'danh sách video', 'manga story'],
                menu: '#menu',
                // scroll logo
                afterLoad: function(anchorLink, index){
                  var loadedSection = $(this);

                  //using index
                  if(index == 1){
                      $(".logo-red").hide();
                      $(".logo-white").show();
                      $(".icon-button").hide();
                  }else{
                      $(".logo-red").show();
                      $(".logo-white").hide();
                      $(".icon-button").show();
                  }
                }     
          });
                    //onclick button
              $('.icon-button').click(function(){
                $.fn.fullpage.moveTo('trang chủ', 1);
              })

              // });

        }else if(screen_width <= 1024){
          var TopFixButton = $('.icon-button');
          var logo = $('.logo');
          var language = $('.language');
          $(window).scroll(function(){
            if($(this).scrollTop()>850){  
              TopFixButton.show();
            }else{
              TopFixButton.hide();
            }
            if($(this).scrollTop()>80){
              logo.hide();
              language.hide();
            }else{
              logo.show();
              language.show();
            }
          })

          $('.icon-button').click(function(){
            $('html, body').animate({
              scrollTop: $('.screen1-right').offset().top
            }, 1000);
          })
        }else if(screen_width < 768){
          $("nav").addClass('navbar');
        }
      // full screen
      
       // end fullscreen
        //click search
            $(".search i").click(function(){
              $(".search input").toggle();             
            })
            //blur search
            $('.search input').blur(function(){
              $(".search input").hide();        
            })
         

        //popup swiper slide

        $('.swiper-slide').hover(function(){
          $('[data-toggle="tooltip"]').tooltip(); 
        });

       
        // click play videos
        
           $(".play-video").click(function(e){
              timer = setTimeout(function() {
                  $("#video")[0].src += "&autoplay=1";
                  e.preventDefault();
              }, 0);
              $(".play-youtube").show();
                  $(".logo-white").hide();
                  $(".screen1-content").hide();          
            })

        ///play youtube
          // (function($){
          //   // var video_id = "<?php echo $video_id; ?>";
          //   // var video_screen1 = $('.screen1');
          //       setTimeout (function(){
                 
          //         // if( $(".screen1-left").hasClass('.screen1-content')){
          //             $(".play-youtube").show();
          //             $(".screen1-left .screen1-content").hide();
          //             $(".logo-white").hide();
          //             $(".screen1-left").siblings('.play-youtube').html('<iframe id="video" src="http://www.youtube.com/embed/HSlD79sqHp4?autoplay=1" width="100%" height="100%" frameborder="0" allowfullscreen></iframe>');

          //         // }else{
          //         //     _this.addClass('hidden-play-video');
          //         // }
          //       }, 5000); 
          // })($);

});  
  




 